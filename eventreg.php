<!DOCTYPE HTML>
<html>
	<head>
		<title>Event Registration</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="landing">
		<div id="page-wrapper">

			<!-- Header -->
			<?php include("header.php"); ?>

			<!-- Banner -->
			<?php include("banner.php"); ?>

			<!-- Main -->
				<section id="main" class="container">
					<section class="box special">
						<header class="major">
							<h3>Event Registration Form</h3>
							<p>
								<form method="post" action="eventreg.php">
									<table>
										<tr style="background-color: rgba(0,0,0,0);">
											<td>
												<input list="events" name="event" placeholder="Event" />
												<datalist id="events">
													<?php
														include_once("config.php");
														$eventselect=mysqli_query($link,'SELECT DISTINCT `event` FROM `sp_team`');
														while($selection=mysqli_fetch_array($eventselect))
															echo
															'<option value="'.$selection['event'].'" />'
													?>
												</datalist>
											</td>
											<td><input type="text" name="no_of_participants" placeholder="Number of Participants" /></td>
											<td><input type="submit" value="Continue" style="float: left;" /></td>
										</tr>
									</table>
								</form>
							</p>
								<?php
									if(isset($_POST['no_of_participants'])){
										if(!empty($_POST['event'])){
											echo
											'<center>
												<form method="post" action="#">';
												$i=0;
												while($i<>$_POST['no_of_participants']){
													echo
													'<input type="text" name="delegate_no" placeholder="Delegate No" style="margin: 7px; width: 50%;" />';
													$i=$i+1;
												}
												echo
												'<input type="submit" value="Submit" style="margin: 10px;" />
											</center>';
										}
										else {
											echo '<center><h4 style="color: #d00;">Please enter event name</h4></center>';
										}
									}
								?>
						</header>
					</section>
				</section>
				<?php include("footer.php"); ?>
		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollgress.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>
