<!DOCTYPE HTML>
<html>
	<head>
		<title>Delegate Card Registration</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="landing">
		<div id="page-wrapper">

			<!-- Header -->
			<?php include("header.php"); ?>

			<!-- Banner -->
			<?php include("banner.php"); ?>

			<!-- Main -->
				<section id="main" class="container">
					<section class="box special">
						<header class="major">
							<h2 style="margin-bottom: 30px;">Delegate Card Registration</h2>
							<form method="post" action="index.php">
									<table>
										<tr style="background-color: rgba(0,0,0,0); border: 0px;">
											<td><input type="text" placeholder="Name" name="name" /></td>
											<td><input type="text" placeholder="Registration Number" name="reg_no" /></td>
										</tr>
										<tr style="background-color: rgba(0,0,0,0); border: 0px;">
											<td><input type="text" placeholder="College" name="college" /></td>
											<td><input type="email" placeholder="E-mail" name="email" /></td>
										</tr>
										<tr style="background-color: rgba(0,0,0,0); border: 0px;">
											<td><input type="text" placeholder="Phone Number" name="ph_no" /></td>
											<td>
												<select name="gender" style="float: left;">
													<option>Gender</option>
													<option value="male">Male</option>
													<option value="female">Female</option>
												</select>
											</td>
										</tr>
										<tr style="background-color: rgba(0,0,0,0); border-bottom: 0px;">
											<td style="text-align: right;"><input style="width: 50%;" type="submit" value="Submit" /></td>
											<td style="text-align: left;"><input style="width: 50%;"  type="reset" value="Reset" /><td>
										</tr>
									</table>
									<?php
										include_once("config.php");
										$name=$_POST['name'];
										$reg_no=$_POST['reg_no'];
										$college=$_POST['college'];
										$email=$_POST['email'];
										$ph_no=$_POST['ph_no'];
										$gender=$_POST['gender'];
										$name=mysqli_real_escape_string($link,$name);
										$reg_no=mysqli_real_escape_string($link,$reg_no);
										$college=mysqli_real_escape_string($link,$college);
										$email=mysqli_real_escape_string($link,$email);
										$ph_no=mysqli_real_escape_string($link,$ph_no);
										$gender=mysqli_real_escape_string($link,$gender);
											if(!empty($name)&&!empty($reg_no)&&!empty($college)&&!empty($email)&&!empty($ph_no)&&!empty($gender))
											{
												$unicheck=mysqli_query($link,"SELECT `registration_number`,`college` FROM `sp_students` WHERE `registration_number`='{$reg_no}' AND `college`='{$college}'");
												$unicheckarr=mysqli_fetch_array($unicheck);
												if(!empty($unicheckarr)) echo 'You have already registered';
												else{
													$insertquery=mysqli_query($link,"INSERT INTO `sp_students`(`name`,`registration_number`,`college`,`email`,`phone_number`,`gender`) VALUES ('{$name}','{$reg_no}','{$college}','{$email}','{$ph_no}','{$gender}')");
													echo '<h4>Thank You for Registering. Your Delegate Number is: <font style="color: #d00;">';
													//$delnoquery=mysqli_query($link,"SELECT `delgate_number` FROM `sp_students` WHERE `registration_number`='".$_POST["reg_no"]."' AND `college`='".$_POST["college"]."'");
													$query="SELECT * FROM sp_students";
													$result=mysqli_query($link,$query);
													if($result)
													{
														while($row=mysqli_fetch_assoc($result))
														{
															$maxid=$row["delegate_number"];
														}														
													}
													//$delnoarr=mysqli_fetch_array($delnoquery);
													echo $maxid.'</font></h4>';
												}
											}
											else echo '<h4 style="color: #d00;" >Please fill all the details</h4>';
									?>
						</header>
					</section>
				</section>
				<?php include("footer.php"); ?>
		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollgress.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>
