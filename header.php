<?php
echo '<!-- Header -->
  <header id="header">
    <h1><a href="index.php">Revels 2016</h1>
    <nav id="nav">
      <ul>
        <li><a href="index.php">Home</a></li>
        <li>
          <a href="#" class="icon fa-angle-down">Menu</a>
          <ul>
            <li><a href="eventreg.php">Event Registration</a></li>
            <li><a href="teamreg.php">Teams</a></li>
            <!--<li>
              <a href="#">Submenu</a>
              <ul>
                <li><a href="#">Option One</a></li>
                <li><a href="#">Option Two</a></li>
                <li><a href="#">Option Three</a></li>
                <li><a href="#">Option Four</a></li>
              </ul>
            </li>-->
          </ul>
        </li>
      </ul>
    </nav>
  </header>';
?>
